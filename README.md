# Jupyter notebooks with GEM-related useful code

Jupyter notebooks with code fragments related to pre/post processing for GEM model

 - **netCDF GEM-AQ example** - a very basic example of how to process netCDF results in Python
 - **netCDF trim wtih shp** - sometimes we need to trim Poland or Cetral Europe 
 - **netCDF rotated grid** - accessing netCDF with irregular grid
 - **Mapnik** - a tool for automatic generation of maps (preferably based on *.shp composition)
 - **RPN_demo** - an interface for direct accessing RPN files (librmn.so required)